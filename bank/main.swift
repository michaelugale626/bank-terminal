//
//  main.swift
//  bank
//
//  Created by Michael Apolonio B. Ugale on 24/4/21.
//

import Foundation

var shouldKeepRunning = true
let theRL = RunLoop.current
var owningList = [] as [Owning]
var userList = [] as [User]
var loggedUser = User()

struct Owning {
    var owningFrom: String!
    var owningTo: String!
    var value: Double!
}

struct User {
    var username: String!
    var balance: Double!
}

while shouldKeepRunning && theRL.run(mode: RunLoop.Mode.default, before: .distantPast) {
    
    if let input = fetchInput(prompt: "> "){
        
        let elements = input.components(separatedBy: " ")
        
        if (elements.count >= 2) {
            
            switch input {
                case  _ where input.lowercased().contains("topup"):
                    
                    if ((loggedUser.username != nil) && isNumeric(value: elements[1])) {
                        
                        loginUser(username: loggedUser.username)
                        printTransferred(value: Double(elements[1])!)
                        topup(value: Double(elements[1])!, username:loggedUser.username)
                        checkBalance()
                        getOwning(username:loggedUser.username, isFrom: false)
                    } else {
                        print("Invalid command")
                    }
                    
                    break
                case  _ where input.lowercased().contains("pay"):
                    
                    if ((loggedUser.username != nil) && (elements.count >= 3)  && isNumeric(value: elements[2])) {
                        let username = elements[1]
                        
                        if (isUserExist(username: username)) {
                            let value = Double(elements[2])!
                            loginUser(username: loggedUser.username)
                        
                            let topupValue = checkIfSomeOneOwnMe(value: value)

                            if (topupValue > 0) {
                                pay(value: topupValue, toUser:username)
                                getTransferred(owningTo:username, value: topupValue)
                            }
                            
                            checkBalance()
                            getOwning(username:loggedUser.username, isFrom: false)
                        } else {
                            print("User does not exist")
                        }
                    } else {
                        print("Invalid command")
                    }
                    break
                case  _ where input.lowercased().contains("login"):
                    let username = elements[1]
                    
                    loginUser(username: username)
                    print("",String(format: "Hello, %@!", loggedUser.username!.capitalized))
                    getOwning(username:loggedUser.username!, isFrom: true)
                    checkBalance()
                    getOwning(username:loggedUser.username!, isFrom: false)
                    
                    break
                default:
                    print("Invalid command")
                    shouldKeepRunning = false
                    break
            }
        } else {
            print("Invalid command")
        }
    }
        
        
}

///Check user balance
func checkBalance() {
    
    loginUser(username: loggedUser.username)
    print("",String(format: "Your balance is %.2f.", (loggedUser.balance > 0) ? loggedUser.balance : 0))
}

/// search all users owning
func getOwning(username:String, isFrom: Bool) {
    
    let list = owningList.filter( { return (isFrom) ? $0.owningTo == username : $0.owningFrom == username } )
    
    for row in list {
        let user = isFrom ? row.owningFrom!.capitalized : row.owningTo!.capitalized
        printOwning(name: user, amount: row.value!, type: isFrom)
    }
}

/// check if user exist
func isUserExist(username: String) -> Bool {
    
    let item = userList.firstIndex( where: {  $0.username == username  } )
    
    if ((item) != nil) {
        return true
    }
    
    return false
}

///login user
func loginUser(username: String) {
    
    let item = userList.firstIndex( where: {  $0.username == username  } )
    
    if ((item) != nil) {
        loggedUser = userList[item!]
    } else {
        addUser(username: username, value: 0)
    }
}

/// process transfer of funds
func pay(value:Double, toUser: String) {
    
    topup(value: value, username: toUser)
    
    var totalBalance = Double()
    let item = userList.firstIndex( where: {  $0.username == loggedUser.username  } )
   
    if ((item) != nil) {
        totalBalance = userList[item!].balance - value
        userList[item!].balance = totalBalance
        checkOwning(owningFrom: loggedUser.username, owningTo: toUser, value: totalBalance)
    }
    
    addOwning(owningFrom: loggedUser.username, owningTo: toUser, value: totalBalance)
    loginUser(username:loggedUser.username)
}

/// store values to be topup to user account
func topup(value:Double, username: String) {
    
    var balance = Double()
        
    let item = userList.firstIndex( where: {  $0.username == username  } )
    
    if ((item) != nil ) {
        balance = userList[item!].balance + value
        userList[item!].balance = balance
        checkOwning(owningFrom: loggedUser.username, owningTo: username, value: value)
    }
}

func checkIfSomeOneOwnMe(value:Double) -> Double {
    
    let item = owningList.firstIndex( where: {  $0.owningTo == loggedUser.username  } )
    var total = value
    
     if ((item) != nil) {
        total = owningList[item!].value + value
        owningList[item!].value = total
        let itemUser = userList.firstIndex( where: {  $0.username == owningList[item!].owningFrom  } )
        userList[itemUser!].balance = userList[itemUser!].balance + value
        printOwning(name: owningList[item!].owningFrom, amount: total, type: true)
     }
    
    return (total > 0) ? total : 0
}

///check user owning values
func checkOwning(owningFrom: String, owningTo: String, value: Double) {
    
    var item = owningList.firstIndex( where: {  $0.owningFrom == owningFrom && $0.owningTo == owningTo  } )
    
    if (owningFrom == owningTo) {
        item = owningList.firstIndex( where: {  $0.owningFrom == owningFrom } )
    }
    
     if ((item) != nil) {
        let orignalValue = owningList[item!].value
        owningList[item!].value = owningList[item!].value + value
        let valueCheck: Double = owningList[item!].value
        let itemUser = userList.firstIndex( where: {  $0.username == owningList[item!].owningTo  } )
        
        if (valueCheck.sign == .minus) {
            userList[itemUser!].balance = userList[itemUser!].balance + owningList[item!].value
        } else {
            owningList.remove(at: item!)
            userList[itemUser!].balance = userList[itemUser!].balance + abs(orignalValue!)
            printTransferred(name: userList[itemUser!].username, amount: abs(orignalValue!))
        }
        
     }
}


/// add new user
func addUser(username: String, value: Double) {
    loggedUser = User(username: username, balance: value)
    userList.append(loggedUser)
}

/// add item to owning
func addOwning(owningFrom: String, owningTo: String, value: Double) {
    
    if (value < 0) {
        
        let item = owningList.firstIndex( where: {  $0.owningFrom == owningFrom && $0.owningTo == owningTo  } )
        
         if ((item) != nil) {
            let itemUser = userList.firstIndex( where: {  $0.username == owningList[item!].owningTo  } )
            userList[itemUser!].balance = userList[itemUser!].balance + value
         } else {
            owningList.append(Owning(owningFrom: owningFrom, owningTo: owningTo, value: value))
         }
    }
}

/// process user owning values
func processOwning(value: Double) {
    
    let item = owningList.firstIndex( where: {  $0.owningFrom == loggedUser.username  } )
    
     if ((item) != nil) {
        owningList[item!].value = owningList[item!].value + value
     }
}

/// print values for transferred values
func printTransferred(value: Double) {
    
    let list = owningList.filter( { return $0.owningFrom == loggedUser.username } )
    
    for row in list {
        if (value < abs(row.value)) {
            printTransferred(name: row.owningTo, amount: value)
        }
    }
}

/// process and get values transferred 
func getTransferred(owningTo:String, value: Double) {
    
    let item = owningList.firstIndex( where: {  $0.owningFrom == loggedUser.username && $0.owningTo == owningTo  } )
    var total = value
    
     if ((item) != nil) {
        total = value + owningList[item!].value
     }
    
    printTransferred(name: owningTo, amount: total)
}

/// check if value is number
func isNumeric(value: String) -> Bool {
    
    return Double(value) != nil
}

/// Pring transferred amount to user
func printTransferred(name: String, amount: Double) {
    
    print("",String(format: "Transferred %.2f to %@", amount, name.capitalized))
}

/// Print user owning
func printOwning(name: String, amount: Double, type: Bool) {
    
    let isfrom = (type) ? "from" : "to"
    
    print("",String(format: "Owing %.2f %@ %@", abs(amount), isfrom, name.capitalized))
}

/// fetch user input
func fetchInput(prompt: String? = nil) -> String? {
    if let prompt = prompt {
        print(prompt, terminator: "")
    }
    return readLine()
}
