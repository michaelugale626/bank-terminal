//
//  bankTests.swift
//  bankTests
//
//  Created by Michael Apolonio B. Ugale on 26/4/21.
//

import XCTest
@testable import bank

class bankTests: XCTestCase {
    
    let testUser = "alice"
    let testUser2 = "bob"
    let testUser3 = "mabu"
    let testAmount:Double = 100

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        userList.append(User(username: testUser, balance: testAmount))
        userList.append(User(username: testUser2, balance: testAmount))
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testStringIfNumber() throws {
        XCTAssertTrue(isNumeric(value: "123"))
    }
    
    func testUserLogin() throws {
        XCTAssertEqual(loggedUser.username,testUser,"logged successful")
    }
    
    func testUserExist() throws {
        XCTAssertTrue(isUserExist(username: testUser), "user exist")
        XCTAssertFalse(isUserExist(username: testUser3), "user does not exist")
    }

}
